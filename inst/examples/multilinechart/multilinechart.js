// !preview r2d3 data=read.csv("multilinechart/data.csv"), d3_version = 4
// https://bl.ocks.org/d3noob/4db972df5d7efc7d611255d1cc6f3c4f

console = d3.window(svg.node()).console;
console.log(data);

// set the dimensions and margins of the graph
var margin = {top: 20, right: 20, bottom: 30, left: 50},
    width = width - margin.left - margin.right,
    height = height - margin.top - margin.bottom;

// parse the date / time
var parseTime = d3.timeParse("%d-%b-%y");

// set the ranges
var x = d3.scaleTime().range([0, width]);
var y = d3.scaleLinear().range([height, 0]);

// format the data
  data.forEach(function(d) {
      d.date = parseTime(d.date);
  });
  
x.domain(d3.extent(data, function(d) { return d.date; }));
y.domain([0, d3.max(data, function(d) {
  return Math.max(d.close, d.open);  })]);

// define the 1st line
var valueline = d3.line()
    .x(function(d) { return x(d.date); })
    .y(function(d) { return y(d.open); });

// define the 2nd line
var valueline2 = d3.line()
    .x(function(d) { return x(d.date); })
    .y(function(d) { return y(d.close); });

// set the margin
svg
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform",
        "translate(" + margin.left + "," + margin.top + ")");

// Add the valueline path.
svg.select("g")
    .append("path")
    .data([data])
    .attr("class", "line")
    .style("stroke", "black")
    .style("fill", "none")
    .attr("d", valueline);
   
// Add the valueline2 path.
svg.select("g")
    .append("path")
    .data([data])
    .attr("class", "line")
    .style("stroke", "red")
    .style("fill", "none")
    .attr("d", valueline2);

// Add the X Axis
svg.select("g")
  .append("g")
  .attr("transform", "translate(0," + height + ")")
  .call(d3.axisBottom(x));

// Add the Y Axis
  svg.select("g")
  .append("g")
  .call(d3.axisLeft(y));
