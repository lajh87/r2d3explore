// !preview r2d3 data=  read.csv("data/gdp_pc_lng_ss.csv", stringsAsFactors = FALSE,  check.names = FALSE),  d3_version = 5

// Define date parser
var parseDate = d3.timeParse("%Y-%m-%d"); // this must match the format of date coming in

// Format the data field
data.forEach(function(d) {
  d.date = parseDate(d.date);
  d.value = +d.value;
});

//console.log(d3.extent(data, function(d) { return d.value}));
var nested = d3.nest()
               .key(function(d) { return d.country; })
               .entries(data);
               
var countries = nested.map(function(d){ return d.key;});

//console.log(JSON.stringify(countries));
//console.log(JSON.stringify(nested));

var margin = { top: 20, right: 80, bottom: 30, left: 50 },
width = width - margin.left - margin.right,
height = height - margin.top - margin.bottom;

// Define scales
var xScale = d3.scaleTime().range([0, width]);
var yScale = d3.scaleLinear().range([height, 0]);
var color = d3.scaleOrdinal().range(d3.schemeCategory10);

// Set the domain of the axes
xScale.domain(d3.extent(data, function(d) { return d.date;}));
yScale.domain([0, d3.max(data, function(d) { return d.value;})]);
color.domain(countries);

// Define axes
var xAxis = d3.axisBottom().scale(xScale);
var yAxis = d3.axisLeft().scale(yScale);

// Define lines
var line = d3.line()
             .curve(d3.curveMonotoneX)
             .x(function(d) {return xScale(d.date);})
             .y(function(d) {return yScale(d.value);});

// Define svg canvas
var chart =svg
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// Place the axes on the chart
chart
  .append("g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + height + ")")
  .call(xAxis);

chart
  .append("g")
  .attr("class", "y axis")
  .call(yAxis);

// Plot the line
chart
  .selectAll(".lines")
  .data(nested)
  .enter()
  .append("path")
  .attr("class", "line")
  .style("fill", "none")
  .attr("d", function(d){return line(d.values)})
  .style("stroke", function(d) {
    return color(d.key);
  });
