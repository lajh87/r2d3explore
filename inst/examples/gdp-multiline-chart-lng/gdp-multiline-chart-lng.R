df <- read.csv("data/gdp_pc_lng_ss.csv", 
               stringsAsFactors = FALSE, check.names = FALSE)

r2d3::r2d3(script = "gdp-multiline-chart-lng/gdp-multiline-chart-lng.js", 
           data= df, 
           d3_version = 5)
tibble::as_tibble(head(df))
