// !preview r2d3 data = "",d3_version = 5
// https://observablehq.com/@d3/line-chart

margin = ({top: 20, right: 30, bottom: 30, left: 40});

var margin = {top: 50, right: 50, bottom: 50, left: 50}, 
    width = width - margin.left - margin.right, 
    height = height - margin.top - margin.bottom; 

var n = 21;

var xScale = d3.scaleLinear()
    .domain([0, n-1]) 
    .range([0, width]); 

var yScale = d3.scaleLinear()
    .domain([0, 1]) 
    .range([height, 0]); 

var line = d3.line()
    .x(function(d, i) { return xScale(i); }) 
    .y(function(d) { return yScale(d.y); }) 
    .curve(d3.curveMonotoneX);

var dataset = d3.range(n).map(function(d) { return {"y": d3.randomUniform(1)() } });

svg
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.select("g")
  .append("g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + height + ")")
  .call(d3.axisBottom(xScale)); 

svg.select("g")
  .append("g")
  .attr("class", "y axis")
  .call(d3.axisLeft(yScale)); 

svg.select("g")
  .append("path")
  .datum(dataset) 
  .attr("class", "line") 
  .style("fill", "none")
  .style("stroke", "black")
  .attr("d", line);  
