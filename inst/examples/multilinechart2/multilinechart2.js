// !preview r2d3 data=read.csv("giniLine.csv",check.names=FALSE), d3_version = 5

// this example shows data in a wide format
// the data section below parses the order month column as a date

// Define date parser
var parseDate = d3.timeParse("%Y-%m-%d %H:%M:%S");

// Format the data field
data.forEach(function(d) {
  d["Order Month"] = parseDate(d["Order Month"]);
});

// Return the property name of keys of the specified object or associative arrays.
var productCategories = d3.keys(data[0]).filter(function(key) {
  return key !== "Order Month" && key !== "metric";
});

// Filter the data to only include a single metric
var subset = data.filter(function(el) {
  return el.metric === "Quantity";
});

// Reformat data to make it more copasetic for d3
var concentrations = productCategories.map(function(category) {
  return {
    category: category,
    datapoints: subset.map(function(d) {
      return { date: d["Order Month"], concentration: +d[category] };
    })
  };
});


var margin = { top: 20, right: 80, bottom: 30, left: 50 },
width = width - margin.left - margin.right,
height = height - margin.top - margin.bottom;

// Define scales
var xScale = d3.scaleTime().range([0, width]);
var yScale = d3.scaleLinear().range([height, 0]);
var color = d3.scaleOrdinal().range(d3.schemeCategory10);

// Define axes
var xAxis = d3.axisBottom().scale(xScale);
var yAxis = d3.axisLeft().scale(yScale);

// Define lines
var line = d3
  .line()
  .curve(d3.curveMonotoneX)
  .x(function(d) {
    return xScale(d["date"]);
  })
  .y(function(d) {
    return yScale(d["concentration"]);
  });

  yScale.domain([0.25, 0.5]);
  color.domain(productCategories);

  // Set the domain of the axes
  xScale.domain(
    d3.extent(subset, function(d) {
      return d["Order Month"];
    })
  );
  
  // Define svg canvas
var chart =svg
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  // Place the axes on the chart
  chart
    .append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

  chart
    .append("g")
    .attr("class", "y axis")
    .call(yAxis);

  var products = chart
    .selectAll(".category")
    .data(concentrations)
    .enter()
    .append("g")
    .attr("class", "category");

  products
    .append("path")
    .attr("class", "line")
    .style("fill", "none")
    .attr("d", function(d) {
      return line(d.datapoints);
    })
    .style("stroke", function(d) {
      return color(d.category);
    });
