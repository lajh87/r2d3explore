// !preview r2d3 data= read.csv("gdp-chart/gdp_pc_wd_ss.csv", check.names = FALSE),  d3_version = 5

var margin = { top: 20, right: 80, bottom: 30, left: 50 },
width = width - margin.left - margin.right,
height = height - margin.top - margin.bottom;

// Define date parser
var parseDate = d3.timeParse("%Y-%m-%d"); // must match input data format

data.forEach(function(d) {
  d.date = parseDate(d.date);
});

// Define scales
var xScale = d3.scaleTime().range([0, width]);
var yScale = d3.scaleLinear().range([height, 0]); 
var color = d3.scaleOrdinal().range(d3.schemeCategory10);

// Set the domain of the axes
xScale.domain(d3.extent(data, function(d) {return d.date;}));
yScale.domain([0, 60000]);

// Define axes
var xAxis = d3.axisBottom().scale(xScale);
var yAxis = d3.axisLeft().scale(yScale);

// Define lines
var line = d3
  .line()
  .curve(d3.curveMonotoneX)
  .x(function(d) {
    return xScale(d["date"]);
  })
  .y(function(d) {
    return yScale(d["United Kingdom"]);
  });

svg
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

svg.select("g")
  .append("g")
  .attr("class", "x axis")
  .attr("transform", "translate(0," + height + ")")
  .call(d3.axisBottom(xScale)); 

svg.select("g")
  .append("g")
  .attr("class", "y axis")
  .call(d3.axisLeft(yScale)); 

svg.select("g")
  .append("path")
  .datum(data) 
  .attr("class", "line") 
  .style("fill", "none")
  .style("stroke", "black")
  .attr("d", line);    
