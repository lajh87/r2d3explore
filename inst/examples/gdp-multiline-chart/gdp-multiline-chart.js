// !preview r2d3 data= read.csv("gdp-chart/gdp_pc_wd_ss.csv", check.names = FALSE),  d3_version = 5


// Define date parser
var parseDate = d3.timeParse("%Y-%m-%d"); // this must match the format of date coming in

// Format the data field
data.forEach(function(d) {
  d["date"] = parseDate(d["date"]);
});

// Return the property name of keys of the specified object or associative arrays.
var countries = d3.keys(data[0]).filter(function(key) {
  return key !== "date";
});

// Reformat data to make it more copasetic for d3
var chart_data = countries.map(function(country) {
  return {
    country: country,
    datapoints: data.map(function(d) {
      return { date: d["date"], value: +d[country] };
    })
  };
});

var margin = { top: 20, right: 80, bottom: 30, left: 50 },
width = width - margin.left - margin.right,
height = height - margin.top - margin.bottom;

// Define scales
var xScale = d3.scaleTime().range([0, width]);
var yScale = d3.scaleLinear().range([height, 0]);
var color = d3.scaleOrdinal().range(d3.schemeCategory10);

// Define axes
var xAxis = d3.axisBottom().scale(xScale);
var yAxis = d3.axisLeft().scale(yScale);

// Define lines
var line = d3
  .line()
  .curve(d3.curveMonotoneX)
  .x(function(d) {
    return xScale(d["date"]);
  })
  .y(function(d) {
    return yScale(d["value"]);
  });

  yScale.domain([0, 60000]); // to some extent of all the variables
  color.domain(countries);

  // Set the domain of the axes
  xScale.domain(
    d3.extent(data, function(d) {
      return d["date"];
    })
  );
  
  // Define svg canvas
var chart =svg
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

  // Place the axes on the chart
  chart
    .append("g")
    .attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

  chart
    .append("g")
    .attr("class", "y axis")
    .call(yAxis);

var products = chart
  .selectAll(".country")
  .data(chart_data)
  .enter()
  .append("g")
  .attr("class", "country");
  
 products
  .append("path")
  .attr("class", "line")
  .style("fill", "none")
  .attr("d", function(d) {
    return line(d.datapoints);
  })
  .style("stroke", function(d) {
    return color(d.country);
  });
